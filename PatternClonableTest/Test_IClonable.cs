﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Otus.PatternClonable;

namespace Otus.PatternClonableTest
{
    [TestClass]
    public class Test_IClonable
    {
        [TestMethod]
        public void Test_Party()
        {
            // Arrange
            Party unknownCustomer = new() { Name = "Going to place a $1000 order" };

            // Act
            var unknownCustomerClone = (Party)unknownCustomer.Clone();

            // Assert
            Assert.AreEqual(unknownCustomer.Name, unknownCustomerClone.Name);
        }

        [TestMethod]
        public void Test_Person()
        {
            // Arrange
            Person man = new() { Name = "John", Surname = "Smith", Age = 50 };

            // Act
            var manClone = (Person)man.Clone();

            // Assert
            Assert.AreEqual(man.Name, manClone.Name);
            Assert.AreEqual(man.Surname, manClone.Surname);
            Assert.AreEqual(man.Age, manClone.Age);
        }

        [TestMethod]
        public void Test_Employee()
        {
            // Arrange
            Employee bigBoss = new() { Name = "John", Surname = "Smith", Age = 50, Position = "CEO" };

            // Act
            var bigBossClone = (Employee)bigBoss.Clone();

            // Assert
            Assert.AreEqual(bigBoss.Name, bigBossClone.Name);
            Assert.AreEqual(bigBoss.Surname, bigBossClone.Surname);
            Assert.AreEqual(bigBoss.Age, bigBossClone.Age);
            Assert.AreEqual(bigBoss.Position, bigBossClone.Position);
        }

        [TestMethod]
        public void Test_Organization()
        {
            // Arrange
            Employee bigBoss = new() { Name = "John", Surname = "Smith", Age = 50, Position = "CEO" };
            Organization company = new() { Name = "Best Devs", TaxId11 = "77123456789", CEO = bigBoss };

            // Act
            var companyClone = (Organization)company.Clone();

            // Assert
            Assert.AreEqual(company.Name, companyClone.Name);
            Assert.AreEqual(company.TaxId11, companyClone.TaxId11);
            Assert.AreEqual(company.CEO.Name, companyClone.CEO.Name);
            Assert.AreEqual(company.CEO.Surname, companyClone.CEO.Surname);
            Assert.AreEqual(company.CEO.Age, companyClone.CEO.Age);
            Assert.AreEqual(company.CEO.Position, companyClone.CEO.Position);
        }
    }
}
