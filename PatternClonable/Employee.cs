﻿namespace Otus.PatternClonable
{
    public class Employee : Person
    {
        private string _position;

        public string Position { get { return _position; } set { _position = value; } }

        public override IMyCloneable MyClone(IMyCloneable clone = null)
        {
            clone = base.MyClone(clone ?? new Employee());

            (clone as Employee)._position = _position;

            return clone;
        }

        public override object Clone()
        {
            return new Employee { Name = this.Name, Surname = this.Surname, Age = this.Age, Position = _position };
        }
    }
}
