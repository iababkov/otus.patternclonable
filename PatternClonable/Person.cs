﻿namespace Otus.PatternClonable
{
    public class Person : Party
    {
        private string _surname;
        private int _age;

        public string Surname { get { return _surname; } set { _surname = value; } }
        public int Age { get { return _age; } set { _age = value; } }

        public override IMyCloneable MyClone(IMyCloneable clone = null)
        {
            clone = base.MyClone(clone ?? new Person());

            (clone as Person)._surname = _surname;
            (clone as Person)._age = _age;

            return clone;
        }

        public override object Clone()
        {
            return new Person { Name = this.Name, Surname = _surname, Age = _age };
        }
    }
}
