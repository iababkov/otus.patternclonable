﻿namespace Otus.PatternClonable
{
    public interface IMyCloneable
    {
        public IMyCloneable MyClone(IMyCloneable clone);
    }
}
