﻿using System;

namespace Otus.PatternClonable
{
    public class Party : IMyCloneable, ICloneable
    {
        private string _name;

        public string Name { get { return _name; } set { _name = value; } }

        public virtual IMyCloneable MyClone(IMyCloneable clone = null)
        {
            if (clone == null)
            {
                clone = new Party();
            }

            (clone as Party)._name = _name;

            return clone;
        }

        public virtual object Clone()
        {
            return new Party { Name = _name };
        }
    }
}
