﻿namespace Otus.PatternClonable
{
    public class Organization : Party
    {
        private string _taxId11;
        private Employee _ceo;

        public string TaxId11 { get { return _taxId11; } set { _taxId11 = value; } }
        public Employee CEO { get { return _ceo; } set { _ceo = value; } }

        public override IMyCloneable MyClone(IMyCloneable clone = null)
        {
            clone = base.MyClone(clone ?? new Organization());

            (clone as Organization)._taxId11 = _taxId11;
            (clone as Organization)._ceo = (Employee)_ceo.MyClone();

            return clone;
        }

        public override object Clone()
        {
            return new Organization { Name = this.Name, TaxId11 = _taxId11, CEO = (Employee)_ceo.Clone() };
        }
    }
}
